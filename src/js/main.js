jQuery(document).ready(function($) {
  'use strict';

  //jQuery for page scrolling feature - requires jQuery Easing plugin
  $(function() {
    $('.navbar-nav li a').bind('click', function(event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top - 50
      }, 1500, 'easeInOutExpo');
      event.preventDefault();
    });
  });

  /* ==============================================
    MODERNIZR
  =============================================== */

  if (!Modernizr.input.placeholder) {
    $('input, textarea').placeholder();
  }

  /* ==============================================
    PULSE FALLBACK
  =============================================== */
  if (!Modernizr.cssanimations) {
    $('.btn').addClass('btn-noanimated');
  }

  /* ==============================================
    FITTEXT
  =============================================== */
  $('.slide-caption .btn').fitText(0.7, {
    minFontSize: '10px',
    maxFontSize: '30px'
  });
  $('.slide-caption h1').fitText(1.3, {
    minFontSize: '45px',
    maxFontSize: '90px'
  });
  $('.slide-caption p').fitText(3, {
    minFontSize: '14px',
    maxFontSize: '20px'
  });

  /* ==============================================
    WAYPOINTS
  =============================================== */
  // Only load parallax when not on mobile devices
  if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

    $('.animated').waypoint(function() {
      var animation = $(this.element).attr('data-animation');
      $(this.element).addClass(animation);
      $(this.element).addClass('visible');
    }, {
      offset: '50%',
      triggerOnce: true
    });

  } else {

    $('.animated').addClass('visible');

  }

  $('#about').waypoint(function() {
    $('#about .progress-bar').each(function() {
      var percent = $(this).attr('data-value') + '%';
      $(this).css('width', percent);
    });
  }, {
    offset: '70%',
    triggerOnce: true
  });

  /* ==============================================
    MMENU
   =============================================== */

  function mmenuw() {
    var wi = $(window).width();
    var nb = $('#mmenu-side-menu').length;
    if (wi < 760) {
      if (nb < 1) {
        $('.side-menu').clone(true).attr('id', 'mmenu-side-menu').mmenu({
          position: 'right',
          zposition: 'front',
          moveBackground: true,
          clone: true,
          dragOpen: false,
        });
        $('#mmenu-side-menu ul').removeClass('nav navbar-nav navbar-right');
        $('#mmenu-side-menu ul').removeClass('dropdown-menu');
        $('#mmenu-side-menu ul').removeClass('pull-left');
        $('#mmenu-side-menu li').removeClass('dropdown-submenu');
      }
    }
  }

  function mmenuwClass() {
    var wi = $(window).width();
    if (wi < 992) {
      $('#nav .container').addClass('container-fluid');
      $('#nav .container').removeClass('container');
    } else {
      $('#nav .container-fluid').addClass('container');
      $('#nav .container-fluid').removeClass('container-fluid');
    }
  }

  mmenuw();
  mmenuwClass();
  $(window).resize(function() {
    mmenuw();
    mmenuwClass();
  });

  /* ==============================================
    Count Factors
   =============================================== */

  (function($) {
    $.fn.countTo = function(options) {
      // merge the default plugin settings with the custom options
      options = $.extend({}, $.fn.countTo.defaults, options || {});

      // how many times to update the value, and how much to increment the value on each update
      var loops = Math.ceil(options.speed / options.refreshInterval),
        increment = (options.to - options.from) / loops;

      return $(this).each(function() {
        var _this = this,
          loopCount = 0,
          value = options.from,
          interval = setInterval(updateTimer, options.refreshInterval);

        function updateTimer() {
          value += increment;
          loopCount++;
          $(_this).html(value.toFixed(options.decimals));

          if (typeof(options.onUpdate) == 'function') {
            options.onUpdate.call(_this, value);
          }

          if (loopCount >= loops) {
            clearInterval(interval);
            value = options.to;

            if (typeof(options.onComplete) == 'function') {
              options.onComplete.call(_this, value);
            }
          }
        }

      });
    };

    $.fn.countTo.defaults = {
      from: 0, // the number the element should start at
      to: 100, // the number the element should end at
      speed: 1000, // how long it should take to count between the target numbers
      refreshInterval: 100, // how often the element should be updated
      decimals: 0, // the number of decimal places to show
      onUpdate: null, // callback method for every time the element is updated,
      onComplete: null, // callback method for when the element finishes updating
    };
  })(jQuery);

  function countUp() {
    var dataperc;
    $('.fact-number').each(function() {
      dataperc = $(this).attr('data-perc'),
        $(this).find('.factor').delay(6000).countTo({
          from: 10,
          to: dataperc,
          speed: 1000,
          refreshInterval: 10,
        });
    });
  }

  $('.fact-number').waypoint(function() {
    countUp();
  }, {
    offset: '70%',
    triggerOnce: true
  });

  /* ==============================================
    BUTTON TO TOP
  =============================================== */
  // fade in .back-top
  $(function() {
    // scroll body to 0px on click
    $('.back-top').click(function() {
      $('body,html').animate({
        scrollTop: 0
      }, 3000);
      return false;
    });
  });

  /* ==============================================
    OWL CAROUSEL
  ==================================================  */

  var $slidesOwlContainer = $('.slider-section');
  var $slidesOwlSlides = $slidesOwlContainer.children('li');

  if ($slidesOwlSlides.length > 1) {
    $slidesOwlContainer.owlCarousel({
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      nav: true,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      items: 1,
      margin: 0,
      loop: true,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: false,
      dots: false,
      stagePadding: 0,
      smartSpeed: 1000,
      responsive: {
        0: {
          nav: false
        },
        420: {
          nav: false
        },
        768: {
          nav: false
        },
        1000: {
          nav: true
        },
      }
    });
  } else {
    $slidesOwlContainer.css('display', 'block');
  }


  var $quoteOwlContainer = $('#quote-slider');
  var $quoteOwlSlides = $quoteOwlContainer.children('div');

  // More than one slide - initialize the carousel
  if ($quoteOwlSlides.length > 1) {
    $quoteOwlContainer.owlCarousel({
      items: 1,
      loop: true,
      dots: true,
      nav: false,
      autoPlay: false,
      slideSpeed: 1000,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    });
  } else {
    $quoteOwlContainer.css('display', 'block');
  }

  var $partnersOwlContainer = $('#partners-slider');
  var $partnersOwlSlides = $partnersOwlContainer.children('div');

  if ($partnersOwlSlides.length > 1) {
    $partnersOwlContainer.owlCarousel({
      dots: false,
      nav: false,
      autoPlay: false,
      slideSpeed: 1000,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      responsive: {
        0: {
          items: 2
        },
        420: {
          items: 2
        },
        768: {
          items: 3
        },
        1000: {
          items: 5
        },
        1400: {
          items: 6
        },
        1600: {
          items: 7
        }
      }
    });
  } else {
    $partnersOwlContainer.css('display', 'block');
  }

  var $blogOwlContainer = $('.blog-slider');
  var $blogOwlSlides = $blogOwlContainer.children('article');

  if ($blogOwlSlides.length > 1) {
    $blogOwlContainer.owlCarousel({
      dots: false,
      nav: true,
      autoPlay: true,
      slideSpeed: 1000,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      responsive: {
        0: {
          items: 1
        },
        420: {
          items: 1
        },
        768: {
          items: 3
        },
        1400: {
          items: 5
        },
        1600: {
          items: 6
        }
      }
    });
  } else {
    $blogOwlContainer.css('display', 'block');
  }

  /* ==============================================
    TOOLTIP
  =============================================== */
  jQuery('[data-toggle~="tooltip"]').tooltip({
    container: 'body'
  });

  /* ==============================================
    MIXITUP PORTFOLIO
  =============================================== */
  //Mixitup
  $(function() {
    $('.home-projects').mixItUp();
  });

  /* ==============================================
    MAGNIFIC POPUP
  =============================================== */
  $('.popup').magnificPopup({
    type: 'image',
    gallery: {
      enabled: true
    },
    callbacks: {
      afterClose: function() {
        $('html').css('overflow', 'hidden');
      }
    }
  });

  /* ==============================================
    NAV
  =============================================== */
  $('#nav').sticky({
    topSpacing: 0
  });

  /* ==============================================
    AJAX CONTACT FORM
  =============================================== */
  $('#contactform').submit(function() {
    var action = $(this).attr('action');
    $('.message').slideUp(750, function() {
      $('.message').hide();
      $('#submit')
        .after('<img src="images/ajax-loader.gif" class="loader" />')
        .attr('disabled', 'disabled');
      $.post(action, {
          name: $('#name').val(),
          email: $('#email').val(),
          subject: $('#subject').val(),
          comments: $('#comments').val(),
        },
        function(data) {
          $('message').html(data);
          $('.message').slideDown('slow');
          $('#contactform img.loader').fadeOut('slow', function() { $(this).remove(); });
          $('#submit').removeAttr('disabled');
          if (data.match('success') !== null) $('#contactform').slideUp('slow');
        }
      );
    });
    return false;
  });

});
//End Document.ready


$(window).on('load', function() {
  'use strict';

  /* ==============================================
    PAGE LOADER
  =============================================== */

  $('.loader-item').fadeOut();
  $('.pageloader').fadeOut('slow');

});
//End window.load
