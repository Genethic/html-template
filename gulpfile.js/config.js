// ==== CONFIGURATION ==== //

var name = 'name', // The directory name for your theme; change this at the very least!
  src = './src/',
  build = './build/',
  dist = './dist/',
  tmp = './tmp/',
  bower = './bower_components/',
  hostname = 'ssh_hostname', // for rsync
  localPath = '/var/www/html/wordpress/sites/name/', // for rsync
  onlinePath = '/var/www/html/demos/name'; // for rsync

module.exports = {

  name: name,
  author: 'Laura Moreno',
  version: '1.0.0',
  src: src,
  build: build,
  dist: dist,
  tmp: tmp,
  bower: bower,

  // error message for plumber plugin
  errors: {
    title: 'Gulp',
    message: 'Error: <%= error.message %>',
    sound: true
  },

  // files paths by extension
  html: {
    src: src + '**/*.hbs',
    build: build + '**/*.html',
    dist: dist + '/*.html',
  },

  php: {
    src: src + '**/*.php',
    build: build + '**/*.php',
    dist: dist + '/*.php',
  },

  css: {
    src: src + 'css/**/*.{css,scss}',
    build: build + 'css',
    dist: dist + 'css/',
    vendors: build + 'css/vendors',
  },

  js: {
    src: src + 'js/**/*.js',
    build: build + 'js',
    dist: dist + 'js/',
    vendors: build + 'js/vendors',
  },

  images: {
    src: src + 'images/**/*(*.png|*.jpg|*.jpeg|*.gif|*.svg)',
    build: build + 'images',
  },

  // htmlmin plugin configuration
  // used in dist:html-min task
  htmlmin: {
    collapseWhitespace: true,
  },

  // imagemin plugin configuration
  // used in images task
  imagemin: {
    optimizationLevel: 7,
    progressive: true,
    interlaced: true,
  },

  // autoprefixer plugin configuration
  // used with postcss plugin
  // https://github.com/ai/browserslist
  autoprefixer: {
    browsers: ['last 2 version'],
  },

  changelog: {
    preset: 'angular',
  },

  // browsersync plugin configuration
  // used in start and start:dist tasks
  browsersync: {
    build: {
      server: build,
      notify: false,
      open: true,
      port: 3000,
      ui: {
        port: 3001,
      },
    },
    dist: {
      server: {
        baseDir: dist,
        index: 'index.html',
      },
      notify: false,
      open: true,
      port: 3002,
      ui: {
        port: 3003,
      },
    },
  },

  // rsync plugin configuration
  // used in deploy, deploy:local and fsync tasks
  rsync: {
    build: {
      root: 'build',
      destination: localPath,
      archive: true,
      silent: false,
      compress: true,
      clean: true,
    },
    dist: {
      root: 'dist',
      username: 'genethic',
      hostname: hostname,
      port: '4291',
      destination: onlinePath,
      archive: true,
      silent: false,
      compress: true,
      clean: true,
    },
  },

  // uncss plugin configuration
  // used in uncss task
  uncss: {
    html: ['./build/**/*.html'],
  },

  csscolors: {
    withoutGrey: true, // set to true to remove rules that only have grey colors
    withoutMonochrome: true, // set to true to remove rules that only have grey, black, or white colors
  },

};
