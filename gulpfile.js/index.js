/**
 *
 * Gulp Workflow for HTML projects
 *
 * @author Laura Moreno
 * @version 1.0.0
 *
 */

var gulp = require('gulp');
var requireDir = require('require-dir');
var list = require('gulp-task-listing');
var runSequence = require('run-sequence').use(gulp);

// Require all tasks in 'tasks' folder, including subfolders
requireDir('./tasks', { recurse: true });


/* ========================================================
 *
 * List all the available commands
 *
 ========================================================== */

gulp.task('default', list.withFilters(/:/));


/* ========================================================
 *
 * Generate the `build` folder
 *
 ========================================================== */

gulp.task('build', function(cb) {
  runSequence('build:del', ['bower', 'html', 'css', 'js', 'php', 'images'], 'modernizr', 'fix-paths', 'inject', cb);
});


/* ========================================================
 *
 * Generate the `dist` folder
 *
 ========================================================== */

gulp.task('dist', ['build'], function(cb) {
  runSequence('dist:del', 'dist:copy', 'dist:clean', 'dist:html-min', 'dist:css-min', 'dist:js-min', 'dist:inject', cb);
});


/* ========================================================
 *
 * Create a zip folder that include the template ready to distribute
 *
 ========================================================== */

gulp.task('zip', function(cb) {
  runSequence('z:files', 'z:preclean', 'z:files-zip', 'z:docs', 'z:docs-zip', 'z:files-del', 'z:all', 'z:clean', cb);
});


/* ========================================================
 *
 * New version pre-release steps
 *
 ========================================================== */

gulp.task('release', function(cb) {
  runSequence('r:version', 'r:changelog', 'r:commit', 'r:push', 'r:tag', cb);
});


/* ========================================================
 *
 * Copy vendors files from the `./bower_components` folder to the `build` folder.
 * CSS and JS files that are not already minified will be minified and renamed with `.min` suffix.
 *
 ========================================================== */

gulp.task('bower', [
  'b:css', 'b:js', 'b:fonts'
]);


/* ========================================================
 *
 * Lint all the SASS, JS and PHP files
 *
 ========================================================== */

gulp.task('lint', [
  'lint:html', 'lint:sass', 'lint:js', 'lint:php'
]);
