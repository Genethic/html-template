/* =========================================================
 *
 * HTML Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `html` - handlebars
 *  2. `inject` - Automatically inject paths to CSS and JS files in 'build' into html files.
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var handlebars = require('gulp-compile-handlebars');
var rename = require('gulp-rename');
var data = require('gulp-data');
var path = require('path');
var plumber = require('gulp-plumber');
var inject = require('gulp-inject');
var series = require('stream-series');
var browserSync = require('browser-sync').create();
var bsStream = browserSync.stream();

// include configuration file
var config = require('./../config');

// declare error function
var onError = function(err) {
  notify.onError(config.errors)(err);
  this.emit('end');
};


/* ==================
 *  Tasks
 ================== */

gulp.task('html', function() {
  var options = {
    batch: [
      './src/templates/layouts',
      './src/templates/partials',
    ],
  };
  return gulp.src('src/*.hbs')
    .pipe(data(function(file) {
      return require('./../../src/content.json');
    }))
    .pipe(handlebars(null, options))
    .pipe(rename(function(path) {
      path.extname = '.html';
    }))
    .pipe(gulp.dest(config.build));
});

gulp.task('inject', ['html'], function() {
  var vendorsCss = gulp.src([config.css.vendors + '/**/*.css']);
  var customCss = gulp.src([config.css.build + '/*.css']);
  var jquery = gulp.src([config.js.vendors + '/**/jquery.min.js']);
  var vendorsJs = gulp.src([config.js.vendors + '/**/*.js', '!' + config.js.vendors + '/**/jquery.min.js']);
  var customJs = gulp.src([config.js.build + '/*.js']);
  /*remove the dist word from the url*/
  var options = {
    addRootSlash: false,
    ignorePath: ['src', 'dist', 'build']
  };
  return gulp.src(config.html.build)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(inject(series(vendorsCss, customCss), options))
    .pipe(inject(series(jquery, vendorsJs, customJs), options))
    .pipe(gulp.dest(config.build))
    .pipe(browserSync.stream());
});
