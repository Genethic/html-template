/* =========================================================
 *
 * Images Tasks.
 *
 * Generate a distribution ready folder.
 *
 *  1. `images` - copy images from 'src' to 'build'
 *  2. `favicon` - copy favicon from 'src' to 'build'
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var newer = require('gulp-newer');
var browserSync = require('browser-sync').create();
var bsStream = browserSync.stream();

// include configuration file
var config = require('./../config');


/* ==================
 *  Tasks
 ================== */

gulp.task('images', ['favicon'], function() {
  return gulp.src(config.images.src)
    .pipe(newer(config.images.build))
    .pipe(imagemin(config.imagemin))
    .pipe(gulp.dest(config.images.build))
    .pipe(browserSync.stream());
});

gulp.task('favicon', function() {
  return gulp.src(config.src + '/images/favicon.png')
    .pipe(newer(config.build))
    .pipe(imagemin(config.imagemin))
    .pipe(gulp.dest(config.build));
});
