/* =========================================================
 *
 * BrowserSync Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `start` - copy build folder content
 *  2. `start:dist` - zip copied build folder
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var bsStream = browserSync.stream();

// include configuration file
var config = require('./../config');


/* ==================
 *  Tasks
 ================== */

gulp.task('start', function() {
  browserSync.init(config.browsersync.build);
  gulp.watch(config.css.src, ['css']);
  gulp.watch(config.js.src, ['js']);
  gulp.watch(config.html.src, ['inject']);
  gulp.watch(config.images.src, ['images']);
});

gulp.task('start:dist', function() {
  browserSync.init(config.browsersync.dist);
});
