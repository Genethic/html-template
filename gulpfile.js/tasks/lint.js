/* =========================================================
 *
 * Lint Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `lint:html` - lint html files
 *  2. `lint:sass` - lint sass files
 *  3. `lint:js` - lint js files
 *  4. `lint:php` - lint php files
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var htmlhint = require('gulp-htmlhint');
var sasslint = require('gulp-sass-lint');
var jshint = require('gulp-jshint');
var phplint = require('gulp-phplint');

// include configuration file
var config = require('./../config');


/* ==================
 *  Tasks
 ================== */

gulp.task('lint:html', function() {
  return gulp.src(config.html.src)
    .pipe(htmlhint())
    .pipe(htmlhint.reporter());
});

gulp.task('lint:sass', function() {
  return gulp.src(config.css.src)
    .pipe(sasslint({ options: { 'config-file': '/home/laura/.sass-lint.yml' } }))
    .pipe(sasslint.format())
    .pipe(sasslint.failOnError());
});

gulp.task('lint:js', function() {
  return gulp.src(config.js.src)
    .pipe(jshint('/home/laura/.jshintrc'))
    .pipe(jshint.reporter('default'));
});

gulp.task('lint:php', function() {
  return gulp.src(config.src + '/**/*.php')
    .pipe(phplint())
    .pipe(phplint.reporter('fail'));
});
