/* =========================================================
 *
 * JS Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `js` - copy build folder content
 *  2. `modernizr` - zip copied build folder
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var modernizr = require('gulp-modernizr');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();
var bsStream = browserSync.stream();

// include configuration file
var config = require('./../config');

// declare error function
var onError = function(err) {
  notify.onError(config.errors)(err);
  this.emit('end');
};


/* ==================
 *  Tasks
 ================== */

gulp.task('js', function() {
  return gulp.src(config.js.src)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(config.js.build))
    .pipe(browserSync.stream());
});

// build custom modernizr.js file
gulp.task('modernizr', function() {
  return gulp.src(config.js.build + '/*.js')
    .pipe(modernizr())
    .pipe(uglify())
    .pipe(rename('modernizr.min.js'))
    .pipe(gulp.dest(config.js.vendors + '/min'));
});
