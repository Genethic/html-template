/* =========================================================
 *
 * PHP Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `php` - copy php files from 'src' to 'build'
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');

// include configuration file
var config = require('./../config');


/* ==================
 *  Tasks
 ================== */

gulp.task('php', function() {
  return gulp.src(config.src + '/**/*.php')
    .pipe(gulp.dest(config.build));
});
