/* ========================================================

 * Zip Tasks.
 *
 * Generate a distribution ready zip file. This tasks must be run on sequence.
 *
 *  1. `z:preclean` - copy build folder content
 *  2. `z:files` - copy build folder content
 *  3. `z:files-zip` - zip copied build folder
 *  4. `z:files-min` - copy dist folder content
 *  5. `z:files-min-zip` - zip copied dist folder
 *  6. `z:files-del` - delete not needed folders
 *  7. `z:docs` - copy documentation folder
 *  8. `z:docs-zip` - zip copied documentation folder
 *  9. `z:all` - zip all in a final folder
 *  10. `z:clean` - delete tmp folder
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var zip = require('gulp-zip');
var del = require('del');

// include configuration file
var config = require('./../config');


/* ==================
 *  Tasks
 ================== */

gulp.task('z:preclean', function() {
  return del([
    config.tmp + '/**/maps/**',
  ]);
});

gulp.task('z:files', function() {
  return gulp.src(config.build + '/**/*')
    .pipe(gulp.dest(config.tmp + '/' + config.name + '/' + config.name));
});

gulp.task('z:files-zip', function() {
  return gulp.src(config.tmp + '/' + config.name + '/**/*')
    .pipe(zip(config.name + '.zip'))
    .pipe(gulp.dest(config.tmp));
});

// gulp.task('z:files-min', function() {
//   return gulp.src(config.dist + '/**/*')
//     .pipe(gulp.dest(config.tmp + '/' + config.name + '-min' + '/' + config.name + '-min'));
// });

// gulp.task('z:files-min-zip', function() {
//   return gulp.src(config.tmp + '/' + config.name + '-min/**/*')
//     .pipe(zip(config.name + '-min' + '.zip'))
//     .pipe(gulp.dest(config.tmp));
// });

gulp.task('z:docs', function() {
  return gulp.src('./docs/**/*')
    .pipe(gulp.dest(config.tmp + '/documentation/documentation'));
});

gulp.task('z:docs-zip', function() {
  return gulp.src(config.tmp + '/documentation/**/*')
    .pipe(zip('documentation.zip'))
    .pipe(gulp.dest(config.tmp));
});

gulp.task('z:files-del', function() {
  return del([
    config.tmp + '/' + config.name + '/**',
    config.tmp + '/' + config.name + '-min' + '/**',
    config.tmp + '/documentation/**',
  ]);
});

gulp.task('z:all', function() {
  return gulp.src(config.tmp + '/**/*')
    .pipe(zip(config.name + '-all.zip'))
    .pipe(gulp.dest('./'));
});

gulp.task('z:clean', function() {
  return del([
    config.tmp + '/**',
  ]);
});
