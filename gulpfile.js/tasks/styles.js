/* ========================================================

 * CSS Task
 *
 *  This task does the following:
 *
 *  1. `css` - copy build folder content
 *   1. Compile Sass to CSS
 *   2. Write Sourcemaps
 *   3. Autoprefixe
 *   4. Merge media queries
 *
 *  2. `fix-paths` - fix font awesome path
 *
 *  3. `uncss` - copy build folder content
 *   1. Compile Sass to CSS
 *   2. Autoprefixe
 *   3. Merge media queries
 *   4. copy to 'cleaning' folder
 *   5. create uncss file
 *   6. rename with 'uncss-' prefix
 *   7. copy to 'cleaning' folder
 *
 *  4. `skins` - copy build folder content
 *  5. `rtl` - copy build folder content
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var uncss = require('gulp-uncss');
var cleanCSS = require('gulp-clean-css');
var csscomb = require('gulp-csscomb');
var rtlcss = require('gulp-rtlcss');
var colorsOnly = require('postcss-colors-only');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
//var mergeRules = require('postcss-merge-rules');
var browserSync = require('browser-sync').create();
var bsStream = browserSync.stream();

// include configuration file
var config = require('./../config');
var csscombConfig = require('../.csscomb.json');

// declare error function
var onError = function(err) {
  notify.onError(config.errors)(err);
  this.emit('end');
};

// postCSS processors
var processors = [
  mqpacker,
  autoprefixer(config.autoprefixer)
];


/* ==================
 *  Tasks
 ================== */

gulp.task('css', function() {
  return gulp.src(config.css.src)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(postcss(processors))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(config.css.build))
    .pipe(browserSync.stream());
});

gulp.task('fix-paths', function() {
  return gulp.src(config.css.vendors + '/**/*.css')
    .pipe(replace(/(\.\.\/)?fonts/g, '../../../fonts'))
    .pipe(gulp.dest(config.css.vendors));
});

gulp.task('uncss', function() {
  return gulp.src(config.css.src)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./cleaning/'))
    .pipe(uncss(config.uncss))
    .pipe(rename({
      prefix: 'uncss-'
    }))
    .pipe(gulp.dest('./cleaning/'));
});

// gulp skin --name skin-name (default:name)
gulp.task('skin', function() {
  var name = gulp.env.name;
  return gulp.src([config.css.src, '!' + config.src + '/css/skins/**'])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(postcss(processors))
    .pipe(postcss([colorsOnly(config.csscolors)]))
    .pipe(cleanCSS({ semanticMerging: true }))
    .pipe(csscomb(csscombConfig))
    .pipe(rename({
      basename: name
    }))
    .pipe(gulp.dest(config.src + '/css/skins/'));
});

gulp.task('rtl', function() {
  return gulp.src(config.css.src)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(postcss(processors))
    .pipe(rtlcss())
    .pipe(rename({
      prefix: 'rtl-'
    }))
    .pipe(gulp.dest('./cleaning/'));
});
