/* ========================================================
 *
 * Realease Tasks.
 *
 * This taks must be run on sequence to automate the release of a new major version
 *
 *  1. `r:version` - increase version number on bower.json and package.json
 *  2. `r:changelog` - add latest commits to CHANGELOG.md
 *  3. `r:commit` - git add all changes and create a prerelease commit
 *  4. `r:push` - git push to bitbucket
 *  5. `r:tag` - create and push a git tag based on package.json version number
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var git = require('gulp-git');
var bump = require('gulp-bump');
var changelog = require('gulp-conventional-changelog');
var fs = require('fs');
var argv = require('yargs').argv;

// include configuration file
var config = require('./../config');



/* ==================
 *  Tasks
 ================== */

// gulp version --type=major|minor|path (default:path)
gulp.task('r:version', function(params) {
  return gulp.src(['./bower.json', './package.json'])
    .pipe(bump({ type: argv.type }))
    .pipe(gulp.dest('./'));
});

gulp.task('r:changelog', function() {
  return gulp.src('CHANGELOG.md', {
      buffer: false
    })
    .pipe(changelog(config.changelog))
    .pipe(gulp.dest('./'));
});

gulp.task('r:commit', function() {
  return gulp.src('.')
    .pipe(git.add({ args: '-A' }))
    .pipe(git.commit('[Prerelease] Bumped version number'));
});

gulp.task('r:push', function(cb) {
  return git.push('bb', 'master', cb);
});

gulp.task('r:tag', function(cb) {
  var version = JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
  git.tag(version, 'Created Tag for version: ' + version, function(error) {
    if (error) {
      return cb(error);
    }
    return git.push('bb', 'master', { args: '--tags' }, cb);
  });
});
