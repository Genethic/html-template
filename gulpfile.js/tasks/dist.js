/* =========================================================
 *
 * Distribution Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `dist:copy` - copy build folder content
 *  2. `dist:clean` - zip copied build folder
 *  3. `dist:html-min` - copy dist folder content
 *  4. `dist:css-min` - zip copied dist folder
 *  5. `dist:js-min` - delete not needed folders
 *  6. `dist:inject` - copy documentation folder
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var del = require('del');
var htmlmin = require('gulp-htmlmin');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var cssminify = require('gulp-csso');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');
var concat = require('gulp-concat');
var series = require('stream-series');
var inject = require('gulp-inject');
var vinylPaths = require('vinyl-paths');

// include configuration file
var config = require('./../config');

// declare error function
var onError = function(err) {
  notify.onError(config.errors)(err);
  this.emit('end');
};


/* ==================
 *  Tasks
 ================== */

gulp.task('build:del', function() {
  return del([
    config.build,
  ]);
});

gulp.task('dist:del', function() {
  return del([
    config.dist,
  ]);
});

gulp.task('dist:copy', function() {
  return gulp.src(config.build + '**/*')
    .pipe(gulp.dest(config.dist));
});

gulp.task('dist:clean', function() {
  return del([
    config.dist + '/**/maps/**',
    config.dist + '/**/css/',
    config.dist + '/**/js/',
  ]);
});

gulp.task('dist:html-min', function() {
  return gulp.src(config.html.dist)
    .pipe(htmlmin(config.htmlmin))
    .pipe(vinylPaths(del))
    .pipe(gulp.dest(config.dist));
});

gulp.task('dist:css-min', function() {
  return gulp.src([
      config.css.build + '/vendors/**/*.css',
      config.css.build + '/*.css'
    ])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(cssminify())
    .pipe(replace(/(\.\.\/)?fonts/g, '../../../fonts'))
    .pipe(concat('all-styles.min.css'))
    .pipe(gulp.dest(config.css.dist));
});

gulp.task('dist:js-min', function() {
  var jquery = gulp.src([config.js.vendors + '/**/jquery.min.js']);
  var customJs = gulp.src([config.js.build + '/*.js']);
  var vendorsJs = gulp.src([
    config.js.vendors + '/**/*.js',
    '!' + config.js.vendors + '/**/jquery.min.js'
  ]);
  return series(jquery, vendorsJs, customJs)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('all-scripts.min.js'))
    .pipe(gulp.dest(config.js.dist));
});

gulp.task('dist:inject', function() {
  var css = gulp.src([config.css.dist + '/**/*.css']);
  var js = gulp.src([config.js.dist + '/**/*.js']);
  var options = {
    addRootSlash: false,
    ignorePath: ['src', 'dist', 'build'] // remove these words from the url
  };
  return gulp.src(config.html.dist)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(inject(css, options))
    .pipe(inject(series(js), options))
    .pipe(gulp.dest(config.dist));
});
