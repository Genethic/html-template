/* =========================================================
 *
 * Deploy Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `deploy` - Synchronize `dist` folder to remote production server
 *  2. `deploy:local` - Synchronize `build` folder  to localhost
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var rsync = require('gulp-rsync');

// include configuration file
var config = require('./../config');


/* ==================
 *  Tasks
 ================== */

gulp.task('deploy', function() {
  return gulp.src(config.dist + '/**/*')
    .pipe(rsync(config.rsync.dist));
});

gulp.task('deploy:local', function() {
  return gulp.src(config.build + '/**/*')
    .pipe(rsync(config.rsync.build));
});
