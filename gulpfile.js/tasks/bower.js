/* =========================================================
 *
 * Bower Tasks.
 *
 * Generate a distribution ready folder. This tasks must be run on sequence.
 *
 *  1. `b:css` - copy build folder content
 *  2. `b:js` - zip copied build folder
 *  3. `b:fonts` - copy dist folder content
 *
 ========================================================== */

// include plugins
var gulp = require('gulp');
var bower = require('main-bower-files');
var cssminify = require('gulp-csso');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var merge = require('merge-stream');

// include configuration file
var config = require('./../config');


/* ==================
 *  Tasks
 ================== */

gulp.task('b:css', function() {
  var nonmin = gulp.src(bower(['**/*.css', '!**/*.min.css']))
    .pipe(cssminify())
    .pipe(rename({
      suffix: '.min'
    }));
  var min = gulp.src(bower('**/*.min.css'));
  return merge(nonmin, min)
    .pipe(gulp.dest(config.css.vendors + '/min'));
});

gulp.task('b:js', function() {
  var nonmin = gulp.src(bower(['**/*.js', '!**/*.min.js']))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }));
  var min = gulp.src(bower('**/*.min.js'));
  return merge(nonmin, min)
    .pipe(gulp.dest(config.js.vendors + '/min'));
});

gulp.task('b:fonts', function() {
  return gulp.src(bower('**/*.{eot,woff,woff2,svg,ttf}'))
    .pipe(gulp.dest(config.build + '/fonts'));
});
